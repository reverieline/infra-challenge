# Infra challenge

## Case

Developers created application, which needs HA infrastructure.

Application (docker image) can be found [here](https://gitlab.com/salamachinas/infra-challenge/container_registry)

## Task

Create HA environment for the application mentioned above. For example, you can create 4 virtual machines. First one for reverse proxy (entry point), other three for the application itself or one virtual machine with [docker-compose](https://docs.docker.com/compose/) for the application and reverse proxy. However architecture solution is completely up to you.

The application listens on port **4000** and supports only one endpoint `/ping` which returns `{"message":"pong"}`.

## Requirements

* There are no strict deadlines, but lets keep in touch - if you have any problems or have no free time available for the task, just contact us;
* Task can be done with:
  - [Vagrant](https://www.vagrantup.com) and [Ansible](https://www.ansible.com) - local solution;
  - [Terrafom](https://www.terraform.io) and [Ansible](https://www.ansible.com) - cloud solution;
* You can use external dependencies or tools if you think that it is needed. We personally recommend to use [Ansible Galaxy](https://galaxy.ansible.com) for external dependencies;
* Documentation must be present (can be a short one):
  - how to run a system (what commands must be executed);
  - how to do maintenance without interrupting users.
* Do not use Paysera name in titles, descriptions or the code itself. This helps others to find the libraries that are really related to our services and/or are developed and maintained by our team.

## Task Submission

You can put the code publicly (in github or similar code control systems) if you want, but please note the requirement about Paysera name usage.

Send it in your favourite format (link to versioned code, code in zip file etc.) to code-infra@paysera.com with subject `DevOps infra challenge`.
