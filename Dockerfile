FROM alpine:3.11

RUN addgroup -g 1000 -S dummy && \
    adduser -u 1000 -S dummy -G dummy -h /usr/local/dummy

USER dummy

WORKDIR /usr/local/dummy

COPY --chown=dummy:dummy service /usr/local/dummy/service

ENTRYPOINT ["/usr/local/dummy/service"]
